#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hidapi.h"

#define VENDOR_ID 0x10c4
#define PRODUCT_ID 0xea90

int main(int argc, char* argv[])
{
    int result; //the status of the transfer
    unsigned char buffer[61]; //this holds the data to send or to read - be sure to clean it after each operation
    hid_device* hid_device; //the handle that identiefies the device that the api should write to

    // Enumerate and print the HID devices on the system
    struct hid_device_info *devs, *cur_dev;
    devs = hid_enumerate(0x0, 0x0);
    cur_dev = devs;    
    while (cur_dev) {
        printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls",
        cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
        printf("\n");
        printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
        printf("  Product:      %ls\n", cur_dev->product_string);
        printf("\n");
        if(cur_dev->vendor_id == VENDOR_ID && cur_dev->product_id == PRODUCT_ID)
            std::cout << "Hoorray - discovered the correct device!" << std::endl;
        cur_dev = cur_dev->next;
    }
    hid_free_enumeration(devs);
    //done with looping devices, now let's get the correct one

    // Initialize the hidapi library
    result = hid_init();
    if(result < 0) return -1;
    
    // Open the device using the VID, PID,
    // and optionally the Serial number.
   
    hid_device = hid_open(VENDOR_ID, PRODUCT_ID, NULL);
    if(hid_device == NULL)
    {
	    std::cerr << "Error opening the HID device" << std::endl;
            return -1;
    }

    //first, let's reset the device
    //buffer[0] = 0x01; //the report ID
    //buffer[1] = 0x01; //the data to reset
    //result = hid_send_feature_report(hid_device, buffer, 2);
    //std::cout << "Return value of Reset command: " << result << std::endl;
    
    //hid_set_nonblocking(hid_device, 1);
    //now, as a test, get the SMBus configuration
    memset(buffer,0, sizeof(buffer));
    buffer[0] = 0x06;
    
    result = hid_get_feature_report(hid_device, buffer, sizeof(buffer)); //the SMBus config contains 13 bytes, the first byte is the report number
    std::cout << "Get feature report return value: "<<result << std::endl;
    if(result != -1) 
    {
        for(size_t i=0; i < result; i++)
            std::cout << "Buffer["<<i << "]= 0x" <<std::hex<< +buffer[i] << std::dec<< std::endl;
    }
    else
        std::cerr << "Error: "<<  hid_error(hid_device) << std::endl;

    return hid_exit();
}

