#include "cp2112.h"
#include <unistd.h>

cp2112::cp2112()
{
}


cp2112::~cp2112()
{
   if(this->device_handle)
   {
      this->cp2112_CloseDevice();
      
   }
} 


bool cp2112::cp2112_FindDevice()
{
   struct hid_device_info *devices, *cur_dev;
   
   devices = hid_enumerate(0x0, 0x0);
   cur_dev = devices; 
   while(cur_dev){
      if(cur_dev->vendor_id == VENDOR_ID && cur_dev->product_id == PRODUCT_ID)
      {    
         printf("Device Found\n  type: %04hx %04hx\n  path: %s\n  serial_number: %ls",
         cur_dev->vendor_id, cur_dev->product_id, cur_dev->path, cur_dev->serial_number);
         printf("\n");
         printf("  Manufacturer: %ls\n", cur_dev->manufacturer_string);
         printf("  Product:      %ls\n", cur_dev->product_string);
         printf("\n");
   
         hid_free_enumeration(devices);         
       
         return true;
      }
      cur_dev = cur_dev->next;
   }   
   hid_free_enumeration(devices);
   return false;
}


void cp2112::cp2112_OpenDevice()
{
   if(this->cp2112_FindDevice())
   {
      int result = hid_init();
      if (result < 0)
      {
         std::cout << "ERROR in cp2112_OpenDevice() --> HID Library initialization failed" << std::endl;
         exit(EXIT_FAILURE);  
      }
 
      this->device_handle = hid_open(VENDOR_ID, PRODUCT_ID, NULL);
   
      if (this->device_handle == NULL)
      {
         std::cout << "ERROR in cp2112_OpenDevice() --> Device opening failed" << std::endl; 
         exit(EXIT_FAILURE);
      }
   }
   else{
      std::cout << "ERROR in cp2112_FindDevice --> Device not found" << std::endl;
   }
}   


void cp2112::cp2112_CloseDevice()
{
   hid_close(this->device_handle);
   hid_exit(); 
}

void cp2112::cp2112_ConfigureDeviceClock()
{
   unsigned char buffer[63]; 
   memset(buffer, 0, sizeof(buffer));
   buffer[0] = SetGet_SMBus_Configuration;
   buffer[1] = 0x00;
   buffer[2] = 0x00;
   buffer[3] = 0xEA;
   buffer[4] = 0x60;
   unsigned int result = hid_send_feature_report(this->device_handle, buffer, sizeof(buffer));

   result = hid_get_feature_report(this->device_handle, buffer, 4);
   if(result > 0)
    {
       for(size_t i=0; i < 5; i++)
         std::cout << "Buffer["<<i << "]= 0x" <<std::hex<< +buffer[i] << std::dec<< std::endl;
    }
    else{
       std::cout << "ERROR in configuring clock" << std::endl;
    }

}

void cp2112::cp2112_ConfigureBias(unsigned char address, unsigned char value)
{
   unsigned char buffer[63];

   memset(buffer, 0, sizeof(buffer));
   buffer[0] = Data_Write;
   buffer[1] = address<<1;
   buffer[2] = 1;
   buffer[3] = value;
  

   unsigned int result = hid_write(this->device_handle, buffer, sizeof(buffer)); 
   if(result < 0)
   { 
      std::cout << "ERROR in cp2112_ConfigureBias --> Writing data failed" << std::endl; 
   }
   else{
      std::cout << "Bias configured" << std::endl;
   }
}

void cp2112::cp2112_ConfigureGain(unsigned char address, unsigned char value)
{  
   unsigned char buffer[63];
   memset(buffer, 0, sizeof(buffer)); 
   buffer[0] = Data_Write; 
   buffer[1] = address<<1; 
   buffer[2] = 1;
   buffer[3] = value;
   unsigned int result = hid_send_feature_report(this->device_handle, buffer, 4);
   if(result < 0)
   {
      std::cout << "ERROR in cp2112_ConfigureGain --> Writing data failed" << std::endl;
   }
   else{
      std::cout << "Gain configured" << std::endl;   
   }
}

void cp2112::cp2112_ResetDevice()
{
   unsigned char buffer[63];
   memset(buffer, 0, sizeof(buffer));
   buffer[0] = Reset_Device;
   buffer[1] = 0x01; 
  
   unsigned int result = hid_send_feature_report(this->device_handle, buffer, 2);
   if(result<0)
   {
      std::cout << "ERROR in cp212_ResetDevice --> Reseting device failed" << std::endl;
   }
   else{
      std::cout << "Device reset" << std::endl;
   }
}

void cp2112::cp2112_GetTransferStatus()
{
 unsigned char buffer[63];
 memset(buffer, 0, sizeof(buffer));
 buffer[0] = Transfer_Status_Request;
 buffer[1] = 0x01;
 unsigned int result = hid_send_feature_report(this->device_handle, buffer, 2);
                 
 memset(buffer, 0, sizeof(buffer));
 buffer[0] = Transfer_Status_Response;
 result = hid_get_feature_report(this->device_handle, buffer, 1);
 if(result > 0)
 {
    for(size_t i=0; i < 5; i++)
       std::cout << "Buffer["<<i << "]= 0x" <<std::hex<< +buffer[i] << std::dec<< std::endl;
 }
 else{
    std::cout << "ERROR in status request" << std::endl;
 }
}

void cp2112::cp2112_GetSMBusConfiguration()
{
 unsigned char buffer[63];
 
 memset(buffer, 0, sizeof(buffer));
 buffer[0] = SetGet_SMBus_Configuration;
 unsigned int result = hid_get_feature_report(this->device_handle, buffer, sizeof(buffer));                               
 if(result > 0)
 {
    std::cout << "SMBus configuration" << std::endl;
    for(size_t i=0; i < result; i++)
       std::cout << "Buffer["<<i << "]= 0x" <<std::hex<< +buffer[i] << std::dec<< std::endl;
 }
 else{
    std::cout << "ERROR in getting SMBus configuration" << std::endl;
 }
}

void cp2112::cp2112_BlinkLED(){
   unsigned char buffer[63];
   int result; 
   
   memset(buffer, 0, sizeof(buffer));
   buffer[0] = SetGet_GPIO_Configuration;
   buffer[1] = 0x01;
   result = hid_send_feature_report(this->device_handle, buffer, sizeof(buffer));
   std::cout << " GPIO config result value " << result << std::endl;
   usleep(100000); 
   memset(buffer, 0, sizeof(buffer));  
   buffer[0] = Set_GPIO; 

   for(int i=0; i<10; i++)
   {  
      buffer[1] = 0x00;
      buffer[2] = 0x01; 
      result = hid_send_feature_report(this->device_handle, buffer, sizeof(buffer));
      usleep(100000);
  
      buffer[1] = 0x01; //LED OFF 
      buffer[2] = 0x01;
      result = hid_send_feature_report(this->device_handle, buffer, sizeof(buffer));
      usleep(100000); 
   }
}
