#ifndef cp2112_h
#define cp2112_h

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <bitset>
#include "hidapi.h"
 
#define VENDOR_ID 0x10c4
#define PRODUCT_ID 0xea90

#define Reset_Device 0x01
#define SetGet_GPIO_Configuration 0x02
#define Get_GPIO 0x03
#define Set_GPIO 0x04
#define Get_Version_Information 0x05
#define SetGet_SMBus_Configuration 0x06

#define Data_Read_Reaquest 0x10
#define Data_Write_Read_Request 0x11
#define Data_Read_Force_Send 0x12
#define Data_Read_Response 0x13
#define Data_Write 0x14
#define Transfer_Status_Request 0x15
#define Transfer_Status_Response 0x16
#define Cancel_Transfer 0x17

#define GetSet_LockByte 0x20
#define GetSet_USB_Configuration 0x21
#define GetSet_Manufacturing_String 0x22
#define GetSet_Product_String 0x23
#define GetSet_Serial_String 0x24

class cp2112
{
  private:
 
  public:
    hid_device* device_handle;
 
    cp2112();
    
    ~cp2112(); 

    //method to find the cp2112 device and put it in the device handle attribute  
    bool cp2112_FindDevice(); 
   
    //method to open the cp2112 device 
    void cp2112_OpenDevice();

    //method to open the cp2112 device
    void cp2112_CloseDevice();

    //method to adapt the cp2112 device clock to the AOH clock
    void cp2112_ConfigureDeviceClock();  

    //method to configure the BIAS
    void cp2112_ConfigureBias(unsigned char, unsigned char);

    //method to configure the GAIN
    void cp2112_ConfigureGain(unsigned char, unsigned char);
   
    //method to reset the device
    void cp2112_ResetDevice();
    
    //method to get the device transfer status
    void cp2112_GetTransferStatus();

    //method to get the device SMBus configuration
    void cp2112_GetSMBusConfiguration();
    
    //method to make LED blink
    void cp2112_BlinkLED();  
};

#endif 
