#include "cp2112.h"
#include "hidapi.h"

#include <stdio.h>
#include <unistd.h>

int main()
{
  cp2112* device = new cp2112;
  unsigned char BiasAdd = 0x61, GainAdd = 0x63; 
  unsigned char BiasVal = 0x23, GainVal = 0x0C;
  
  device->cp2112_OpenDevice();
 
  device->cp2112_ConfigureBias(BiasAdd, BiasVal);
  device->cp2112_ConfigureGain(GainAdd, GainVal);
 
  //device->cp2112_BlinkLED(); //Blinking LED test
  //usleep(1000); 
  delete device; 
  return 0; 
}
