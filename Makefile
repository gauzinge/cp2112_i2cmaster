#all: test

#test: test.o
#	     g++ -o test test.o -I/usr/include/hidapi -L/usr/lib64/ -l hidapi-libusb -l hidapi-hidraw

#test.o: test.cc
#	     g++ -c test.cc -I/usr/include/hidapi -L/usr/lib64/ -l hidapi-libusb -l hidapi-hidraw
		      
#clean:
#	     rm test.o test

all: configureAOH

configureAOH: configureAOH.o cp2112.o
		g++ -std=c++11 -o configureAOH configureAOH.o cp2112.o -I/usr/include/hidapi -L/usr/lib64/ -l hidapi-libusb -l hidapi-hidraw

configureAOH.o: configureAOH.cc cp2112.h
		g++ -std=c++11 -Wall -c configureAOH.cc -I/usr/include/hidapi -L/usr/lib64/ -l hidapi-libusb -l hidapi-hidraw  

cp2112.o: cp2112.cc cp2112.h
		g++ -std=c++11 -Wall -c cp2112.cc -I/usr/include/hidapi -L/usr/lib64/ -l hidapi-libusb -l hidapi-hidraw 

clean:
		rm configureAOH.o configureAOH cp2112.o
